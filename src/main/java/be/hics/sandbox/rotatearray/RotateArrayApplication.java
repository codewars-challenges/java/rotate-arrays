package be.hics.sandbox.rotatearray;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.stream.Collectors;

@SpringBootApplication
public class RotateArrayApplication {

    private final static Object[] s = { 1, 2, 3, 4, 5 };

    public static void main(String[] args) {
        SpringApplication.run(RotateArrayApplication.class, args);
        Arrays.asList(new Rotator().rotate(s, 1)).stream().forEach(System.out::println);
        Arrays.asList(new Rotator().rotate(s, -1)).stream().forEach(System.out::println);
        Arrays.asList(new Rotator().rotate(s, 2)).stream().forEach(System.out::println);
        Arrays.asList(new Rotator().rotate(s, -2)).stream().forEach(System.out::println);
        Arrays.asList(new Rotator().rotate(s, 6)).stream().forEach(System.out::println);
        Arrays.asList(new Rotator().rotate(s, -6)).stream().forEach(System.out::println);
    }
}
