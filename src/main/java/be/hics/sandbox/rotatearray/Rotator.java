package be.hics.sandbox.rotatearray;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

public class Rotator {

    public Object[] rotate(Object[] data, int n) {
        if (Objects.isNull(data) || data.length == 0)
            return null;
        if (n == 0 || (n % data.length) == 0)
            return data;
        int x0 = n > 0 ? data.length - (n % data.length) : Math.abs(n % data.length);
        int x1 = data.length;
        int x2 = 0;
        Stream<Object> s1 = Arrays.stream(Arrays.copyOfRange(data, x0, x1));
        Stream<Object> s2 = Arrays.stream(Arrays.copyOfRange(data, x2, x0));
        return Stream.concat(s1, s2).toArray(size -> new Object[size]);
    }

}